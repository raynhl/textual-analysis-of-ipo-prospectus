# Textual Analysis of IPO Prospectus

This is a program to perform textual analysis of IPO prospectus. It extracts texts of IPO prospectus, converts them into English root words, and calculates similarities, and informative parameters to study he relationship between IPO's prospectus text and opening price of a stock. It is an implementation of the following paper in finance_paper.pdf

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

The program runs on Python 3. First, install the dependencies libraries.

```
pip3 install -r requirements.txt
```


## Running the program

This program is broken into two parts:- (1) Build word universe from list of prospectuses, (2) Calculates similarities and content parameters. 

### Building word universe

The first part of the program involves creating a word universe with the list of IPOs prospectus supplied. In this case, it requires a JSON file (eg. input.json) to supply a list of URLs to extract information from.

```
python3 build_word_universe.py [-i input.json] [-o words.vec]
```

### Calculating similarities and content parameters

The second part of the program compares and calculates textual parameters of a prospectus with respect to other prospectuses. To run the sample example script,

```
python3 compare.py [-i input.json] [-w words.vec]
```

## Authors

* **Ray Ng** - *Developer* - [HomePage](https://www.rayng.xyz)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Authors of [The Information Content of IPO Prospectuses](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1327171)
* Hat tip to anyone whose code was used