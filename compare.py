from modules.prospectus import Prospectus
import json
import sys
import argparse


WHOLE = "whole"
PROSPECTUS_SUMMARY = "prospectus_summary"
RISK_FACTORS = "risk_factors"
USE_OF_PROCEEDS = "use_of_proceeds"
MDA ="management_discussion_and_analysis"

def start_example(args):
	try:
		print("[INFO] Loading prospectus' URLs.")
		urls = json.load(open(args["i"], "r"))["url"]
	except FileNotFoundError:
		print("[ERROR] Please provide valid JSON file containing urls.")
		sys.exit(1)

	try:
		print("[INFO] Loading word universe.")
		universe = json.load(open(args["w"], "r"))
	except FileNotFoundError:
		print("[ERROR] Please provide valid JSON file containing word vectors.")
		sys.exit(1)


	prospectus_list = []

	for index, url in enumerate(urls):
		print("[{}/{}] Processing URL: {}".format(index + 1, len(urls), url))
		prospectus = Prospectus(url, universe = universe)
		prospectus_list.append(prospectus)


	# Example #1: Compare between prospectus_list[0] and prospectus_list[1]
	# Results is similarity measure.

	prospectus1 = prospectus_list[0]
	prospectus2 = prospectus_list[1]
	print("Similarity (whole): {}".format(prospectus1.compare(prospectus2)))		# whole prospectus
	print("Similarity (prospectus_summary): {}".format(prospectus1.compare_section(prospectus2, PROSPECTUS_SUMMARY)))
	print("Similarity (risk_factors): {}".format(prospectus1.compare_section(prospectus2, RISK_FACTORS)))
	print("Similarity (use_of_proceeds): {}".format(prospectus1.compare_section(prospectus2, USE_OF_PROCEEDS)))
	print("Similarity (mda): {}".format(prospectus1.compare_section(prospectus2, MDA)))


	# Refer to page 2823 of the text. In this next example, 
	# IPO i is from prospectus_list[0]
	# K IPOs are prospectus_list[1] and prospectus_list[2]
	# P IPOs is prospectus_list[3] and prospectus_list[4]
	prospectus = prospectus_list[0]
	K_ipos = prospectus_list[1:2]
	P_ipos = prospectus_list[3:4]

	print("")
	print("")
	standard_content, informative_content = prospectus.calculate_standard_and_informative_content(K_ipos, P_ipos)
	print("Standard content: {}".format(standard_content))
	print("informative_content: {}".format(informative_content))



def main():
	parser = argparse.ArgumentParser(description="Example to use prospectus comparator.")
	parser.add_argument("-i", type = str, help = "List of URL to process", default = "input.json")
	parser.add_argument("-w", type = str, help = "Input file containing universe of words.", default = "words.vec")

	try:
		args = vars(parser.parse_args())
	except:
		parser.print_help()
		sys.exit(1)

	start_example(args)
	

if __name__ == "__main__":
	main()