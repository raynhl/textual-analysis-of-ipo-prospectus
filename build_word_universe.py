from modules.prospectus import Prospectus
from modules.language_processor import LanguageProcessor
import json
import sys
import argparse


class UniversalWordVector:
	def __init__(self):
		self.vector = dict()

	def expand(self, rootwords):
		for key, value in rootwords.items():
			if key in self.vector:
				self.vector[key] += value

			else: self.vector[key] = value

	def purge(self):
		for key in list(self.vector):
			if self.vector[key] < 5:
				del self.vector[key]

	def export(self, output_file):
		try:
			with open(output_file, "w") as output:
				output.write(json.dumps(self.vector, sort_keys = True, indent = 4))

		except:
			print("[ERROR] Please provide valid output file path.")

		print("[SUCCESS] Word universe successfully exported to {}".format(output_file))

def build_word_universe(args):
	try:
		urls = json.load(open(args["i"], "r"))["url"]
	except FileNotFoundError:
		print("[ERROR] Please provide valid JSON file containing urls.")
		sys.exit(1)

	vector = UniversalWordVector()
	success_count = 0
	failed_count = 0

	for index, url in enumerate(urls):
		try:
			print("[{}/{}] Processing URL: {}".format(index + 1, len(urls), url))
			prospectus = Prospectus(url)
			language_processor = LanguageProcessor(prospectus.text["whole"])
			vector.expand(language_processor.rootwords)
			success_count += 1
		
		except KeyboardInterrupt:
			print("[EXITING] Ctrl-C pressed. Preparing to exit program now.")
			vector.purge()
			vector.export(args["o"])
			sys.exit(0)

		except:
			failed_count += 1

		finally:
			print("[{}/{}] Success: {} Fail: {} ==> Last URL: {}".format(index + 1, len(urls), success_count, failed_count, url))


	vector.purge()
	vector.export(args["o"])

def main():
	parser = argparse.ArgumentParser(description="Word Universe Builder")
	parser.add_argument("-i", type = str, help = "List of URL to process", default = "input.json")
	parser.add_argument("-o", type = str, help = "Output file for universe of words.", default = "words.vec")

	try:
		args = vars(parser.parse_args())
	except:
		parser.print_help()
		sys.exit(1)

	build_word_universe(args)
	

if __name__ == "__main__":
	main()