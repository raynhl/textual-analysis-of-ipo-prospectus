urllib3==1.22
pandas==0.23.4
numpy==1.13.3
nltk==3.3
beautifulsoup4==4.6.0
pyenchant==2.0.0
statsmodels==0.9.0
scipy==1.1.0
