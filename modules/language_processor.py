from nltk.stem import WordNetLemmatizer
import nltk
import re
import string
import enchant

class LanguageProcessor:
	def __init__(self, text):
		self.EXCLUDED_TAGS = ["PRP", "PRP$", "CC", "IN"]
		self.ARTICLES = ["a", "an", "the"]
		self.lemmatizer = WordNetLemmatizer()
		self.dictionary = enchant.Dict("en_US")
		self.text = text
		self.rootwords = dict()

		self.tags = []
		self.tokenize()		
		self.purge_tags()
		self.lemmatize()

		del self.tags

	def tokenize(self):
		for sentence in nltk.sent_tokenize(self.text):
			tokens = nltk.word_tokenize(sentence)
			self.tags.extend(nltk.pos_tag(tokens))

	def purge_tags(self):
		remaining_tags = []

		for tag in self.tags:
			if tag[1] in self.EXCLUDED_TAGS or tag[1] in string.punctuation or tag[0] in self.ARTICLES:
				continue

			remaining_tags.append(tag)
			
		self.tags = remaining_tags

	def get_wordnet_pos(self, pos):
		from nltk.corpus import wordnet

		if pos.startswith('J'):
			return wordnet.ADJ
		elif pos.startswith('V'):
			return wordnet.VERB
		elif pos.startswith('R'):
			return wordnet.ADV
		else:
			return None

	def lemmatize(self):
		for tag in self.tags:
			wordnet_pos = self.get_wordnet_pos(tag[1])
			word = tag[0].lower()

			has_number = bool(re.search(r'\d', word))
			is_english = self.dictionary.check(word) 
			if not is_english or has_number or len(word) <= 2: 
				continue

			if wordnet_pos is None:
				lemma = self.lemmatizer.lemmatize(word)
			else:
				lemma = self.lemmatizer.lemmatize(word, pos = wordnet_pos)

			if lemma in self.rootwords:
				self.rootwords[lemma] += 1

			else: self.rootwords[lemma] = 1