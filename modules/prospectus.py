import urllib
import re
import sys
import pandas as pd
import numpy as np
import statsmodels.api as sm
from modules.language_processor import LanguageProcessor
from collections import OrderedDict
from bs4 import BeautifulSoup

class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class InputError(Error):
	pass

class Prospectus:
	def __init__(self, url = None, file = None, universe = None):
		if url:	html = urllib.request.urlopen(url).read().decode("utf-8")
		elif file: html = open(file, "r").read().decode("utf-8")
		else:
			raise InputError("Prospectus class constructor: An URL or file to the S-1 document must be supplied.")

		self.soup = BeautifulSoup(html, "lxml")
		self.vector = None
		self.chapters = None
		self.find_chapters()

		self.text = {"whole"			: 	self.extract(),
					"prospectus_summary":	self.extract_chapter("prospectus summary"),
					"risk_factors"		:	self.extract_chapter("risk factors"),
					"use_of_proceeds"	:	self.extract_chapter("use of proceeds"),
					"management_discussion_and_analysis": self.extract_chapter("management's discussion and analysis")	
					}
	
		if universe: 
			self.construct_word_vectors(universe)


	def visible(self, element):
		if element.parent.name in ["style", "script", "[document]", "head", "title"]:
			return False
		elif re.match("<!--.*-->", str(element.encode("utf-8"))):
			return False

		return True


	def find_chapters(self):
		a_tags = self.soup.findAll("a", href = True)

		chapters = dict()
		for a_tag in a_tags:
			if not chapters.get(a_tag.text.strip()):

				not_fin = re.match(r"^.(?!fin).*", a_tag["href"])
				if not_fin is None:
					chapter_number = 0

				else:
					match = re.search("_(\\d+)([a-z]*)", a_tag["href"])
					
					if match is not None:

						chapter_number = int(match.group(1))
						
						if match.group(2): 
								chapter_number += (0.01 * (ord(match.group(2)) - ord('a') + 1))
							
					else: 
						chapter_number = 0

				chapters[a_tag.text.strip().replace("\x92", "'").lower()] = dict({"link": a_tag["href"], "order": chapter_number})

		self.chapters = OrderedDict(sorted(chapters.items(), key=lambda kv: kv[1]["order"]))


	def extract(self):
		results = filter(self.visible, self.soup.findAll(text=True))
		
		string = ""
		
		for result in results:
			string += str(result.string)

		return string

	def loop_until_tag(self, text, start_tag, stop_tag):
		while (start_tag.next != stop_tag):
			if start_tag.string and start_tag.name != "a": 
				text += start_tag.string

			if start_tag.next == stop_tag:
				return text

			start_tag = start_tag.next
			if start_tag is None:
				break

		return text

	def extract_chapter(self, title):
		chapter_titles = list(self.chapters.keys())
		for key in chapter_titles:
			if title in key:
				chapter_key = key
				break

		start_index = list(chapter_titles).index(chapter_key)
		end_index = start_index + 1

		start_tag = self.soup.find("a", {"name": self.chapters[chapter_titles[start_index]]["link"][1:]})
		end_tag = start_tag.find_next("a", {"name": self.chapters[chapter_titles[end_index]]["link"][1:]})

		chapter_text = self.loop_until_tag("", start_tag, end_tag)

		chapter_text = chapter_text.replace("\x92", "'")
		chapter_text = chapter_text.replace("\x93", "\"")
		chapter_text = chapter_text.replace("\x94", "\"")
		chapter_text = chapter_text.replace("\x97", "--")
		chapter_text = chapter_text.replace("\x9f", "")
		
		return chapter_text

	def construct_word_vectors(self, universe):
		self.vector = pd.DataFrame.from_dict(universe, orient = "index", columns = ["universe"])
		
		for key, value in self.text.items():
			lp = LanguageProcessor(value)
			section_rootwords = pd.DataFrame.from_dict(lp.rootwords, orient = "index")
			self.vector[key] = section_rootwords.loc[self.vector.index.intersection(section_rootwords.index)]

		self.vector.fillna(0, inplace = True)

		for column in self.vector:
			self.vector[column] = self.vector[column] / self.vector[column].sum()

	def compare_section(self, prospectus, section):
		if (self.vector is None or prospectus.vector is None):
			return None

		vector_1 = self.vector[section].values / np.sqrt(sum(self.vector[section].values**2))
		vector_2 = prospectus.vector[section].values / np.sqrt(sum(prospectus.vector[section].values**2))

		return np.dot(vector_1, vector_2)

	def compare(self, prospectus):
		return self.compare_section(prospectus, "whole")

	def get_word_vector(self, section):
		return self.vector[section]

	def calculate_standard_and_informative_content(self, recent_ipos, past_industry_ipos):
		df = pd.DataFrame(0, index = self.vector.index, columns = ["norm_rec_i", "norm_ind_i"])

		for recent_ipo in recent_ipos:
			df["norm_rec_i"] += recent_ipo.vector["whole"]

		df["norm_rec_i"] = df["norm_rec_i"] / len(recent_ipos)

		for past_industry_ipo in past_industry_ipos:
			df['norm_ind_i'] += past_industry_ipo.vector["whole"]

		df["norm_ind_i"] = df["norm_ind_i"] / len(past_industry_ipos)

		regression = sm.OLS(self.vector["whole"], df, missing = "drop").fit()
		standard_content = regression.params["norm_rec_i"] + regression.params["norm_ind_i"] 
		informative_content = regression.resid.abs().sum()
		return standard_content, informative_content